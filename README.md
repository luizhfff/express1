# express1 #

## Initial setup ###

### Main enviroment (master branch) ###
* git clone https://gitlab.com/luizhfff/express1.git
* cd express1
* deploy/express1.sh -d
* cd

### Test enviroment (test branch) ###
* mkdir express1test
* cd express1test
* git clone https://gitlab.com/luizhfff/express1.git
* cd express1
* git checkout test
* deploy/express1-test.sh -d
* cd

### NGINX server (Production machine) ###
* cd express1test
* cd express1
* git checkout test
* cd deploy
* cp nginx-redirect.conf ~/etc/nginx/conf.d
* cp nginx-redirect-test.conf ~/etc/nginx/conf.d
* cp nginx-tls.conf ~/etc/nginx/conf.d
* cp nginx-tls-test.conf ~/etc/nginx/conf.d
* ./nginx.sh -d